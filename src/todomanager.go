package src

import "sync"

type ToDo struct {
	ID         string `json:"id"`
	Title      string `json:"title"`
	IsComplete bool   `json:"isComplete"`
}

type ToDoRequestCreate struct {
	Title string `json:"title"`
}

type ToDoManager struct {
	todos []ToDo
	m     sync.Mutex //to make all our operations atomic
}

func MakeNewToDoManager() (td ToDoManager) {
	td = ToDoManager{
		make([]ToDo, 0),
		sync.Mutex{},
	}
	return
}
