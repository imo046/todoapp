package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"todoapp/db"
	"todoapp/src"
)

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func disconnectClient(client *mongo.Client) {
	err := client.Disconnect(context.TODO())
	checkErr(err)
	fmt.Println("Connection closed!")
}

func main() {
	//set up a server
	//e := echo.New()

	//init todomanager
	//tm := src.MakeNewToDoManager()

	//mongo
	cred := src.Credentials{"root", "root"}
	root := cred.Init()

	clientOptions := options.Client().ApplyURI("mongodb://localhost:28017").SetAuth(root)

	mongoInter, err := db.Create(clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	connectionErr := mongoInter.Connect()
	if connectionErr != nil {
		log.Fatalln(connectionErr)
	}
	defer mongoInter.Disconnect()

	_, dbErr := mongoInter.CreateCollection("test", "todos")
	if dbErr != nil {
		log.Fatal(dbErr)
	}

	/***
		//EchoGroups
		authGroup := e.Group("/todos", func(next echo.HandlerFunc) echo.HandlerFunc {
			//Authorization is usually done with tokens, common pattern is to pass auth token as a header in every request
			return func(c echo.Context) error {
				//check auth token
				authorization := c.Request().Header.Get("authorization")
				if authorization != "auth-token" { //fill in token
					c.Error(echo.ErrUnauthorized)
					return nil
				}
				//if token exists, keep moving forward
				next(c)
				return nil
			}
		},
		)

		//middleware logger, function which runs before the actual request function
		//TODO: make a log file
		e.Use(middleware.Logger())

		e.GET("/", func(c echo.Context) error {
			todos := tm.GetAll()
			return c.JSON(http.StatusOK, todos)
		})

		//Create new todos
		authGroup.POST("/create", func(c echo.Context) error {
			requestBody := src.ToDoRequestCreate{}

			//parses the body and "binds" it to the passed type
			err := c.Bind(&requestBody)
			if err != nil {
				return err
			}
			todo := tm.Create(requestBody)
			return c.JSON(http.StatusCreated, todo)
		})

		authGroup.PATCH("/:id/complete", func(c echo.Context) error {
			id := c.Param("id") //get the path param, similar to c.QueryParam
			err := tm.Complete(id)
			if err != nil {
				c.Error(err)
				return err
			}
			return nil
		})

		authGroup.DELETE("/delete/:id", func(c echo.Context) error {
			id := c.Param("id")
			err := tm.Delete(id)
			if err != nil {
				c.Error(err)
				return err
			}
			return nil
		})

		//start
		//wrap with logger fatal
		e.Logger.Fatal(e.Start(":8888"))
	**/
}
