package src

import "go.mongodb.org/mongo-driver/mongo/options"

func MakeGroup(addr string) {

}

type Credentials struct {
	Username string
	Password string
}

func (c Credentials) Init() (cred options.Credential) {
	cred.Username = c.Username
	cred.Password = c.Password
	return cred
}
