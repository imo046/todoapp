package db

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoInterface struct {
	client *mongo.Client
}

func Create(options *options.ClientOptions) (*MongoInterface, error) {
	client, err := mongo.Connect(context.TODO(), options)
	return &MongoInterface{client: client}, err
}

func (m *MongoInterface) Disconnect() error {
	err := m.client.Disconnect(context.TODO())
	if err != nil {
		return err
	}
	fmt.Println("Connection closed!")
	return nil
}

func checkConnection(client *mongo.Client) error {
	err := client.Ping(context.TODO(), nil)
	if err != nil {
		return err
	}
	return nil
}

func (m *MongoInterface) Connect() error {
	//myClient, err := mongo.Connect(context.TODO(), m.clientOptions)
	//defer func(client *mongo.Client) {
	//	disconnectErr := disconnect(client)
	//	if disconnectErr != nil {
	//		log.Fatal(err)
	//	}
	//}(myClient)
	//if err != nil {
	//	return err
	//}
	connectionErr := checkConnection(m.client)
	if connectionErr != nil {
		return connectionErr
	}
	fmt.Println("Connected!")
	return nil
}

func (m *MongoInterface) CreateCollection(dbName, collectionName string) (*mongo.Collection, error) {
	var collection *mongo.Collection
	collection = m.client.Database(dbName).Collection(collectionName)
	if collection == nil {
		return nil, errors.New(fmt.Sprintf("Failed to create a collection %v in the database %v", collectionName, dbName))
	}
	return collection, nil
}
