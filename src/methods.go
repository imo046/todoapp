package src

import (
	"github.com/labstack/echo/v4"
	"strconv"
	"time"
)

// Delete
func (tm *ToDoManager) Delete(ID string) error {
	tm.m.Lock()
	defer tm.m.Unlock()

	var index int = -1
	for i, t := range tm.todos {
		if t.ID == ID {
			index = i
			break
		}
	}
	if index == -1 {
		return echo.ErrNotFound
	}

	return nil
}

// Complete Todos
func (tm *ToDoManager) Complete(ID string) error {
	tm.m.Lock()
	defer tm.m.Unlock()

	var todo *ToDo
	var index int

	for i, t := range tm.todos {
		if t.ID == ID {
			todo = &t
			index = i
			break
		}
	}
	if todo == nil {
		return echo.ErrNotFound
	}

	//check if already completed
	if todo.IsComplete {
		err := echo.ErrBadRequest
		err.Message = "This todo is already complete"
		return err
	}

	//update todos status
	tm.todos[index].IsComplete = true

	return nil

}

// GetAll
func (tm *ToDoManager) GetAll() []ToDo {
	return tm.todos
}

// Create
func (tm *ToDoManager) Create(todoRequest ToDoRequestCreate) ToDo {
	tm.m.Lock()
	//unlock at the end of the execution
	defer tm.m.Unlock()

	newToDo := ToDo{
		strconv.FormatInt(time.Now().UnixMilli(), 10),
		todoRequest.Title,
		false,
	}
	tm.todos = append(tm.todos, newToDo)
	return newToDo
}
